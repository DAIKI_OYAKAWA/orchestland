﻿using UnityEngine;
using System.Collections;

public class MusicPlayer : MonoBehaviour {
	public float bpm_default = 120f;
	public int beat_change = 4;
	public int beat_unit = 5;
	public TempoCheck tempo_check;

	AudioSource source;
	float bpm_now;
	int beat_count = -99;
	int beat = 0;
	float beat_f = 0f;
	// Use this for initialization
	void Start () {
		source = GetComponent<AudioSource> ();
		bpm_now = bpm_default;
	}
	
	// Update is called once per frame
	void Update () {
		float time = source.time;
		beat_f = time / (60f / bpm_default);
		beat = Mathf.FloorToInt (beat_f);
		if (beat_count != beat/beat_change) {
			bpm_now = Mathf.Floor (tempo_check.GetTempoAverage ()/beat_unit)*beat_unit;
			if (bpm_now > 80 && bpm_now < 200) {
				source.pitch = bpm_now / bpm_default;
			}

			beat_count = beat/beat_change;
		}
	}

	public float GetBPM(){
		return bpm_now;
	}

	public int GetBeat(){
		return beat;
	}
	public float GetBeatFloat(){
		return beat_f;
	}
}
