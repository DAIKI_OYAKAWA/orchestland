﻿using UnityEngine;
using System.Collections;

public class TempoCheck : MonoBehaviour {
	public int maxAverageValue = 8;

	float time_old = 0;
	float[] tempo_ary;
	int ary_num = 0;
	float tempo_average = 120f;
	// Use this for initialization
	void Start () {
		tempo_ary = tempo_ary = new float[maxAverageValue];
		for (int i = 0; i < tempo_ary.Length; i++) {
			tempo_ary [i] = 120f;
		}
	}
	
	// Update is called once per frame
	void Update () {
	}

	public void Beat(){
		float time_now = Time.time;
		float time_delta = time_now - time_old;
		float tempo_now = 60f / time_delta;
		if (tempo_now > 80 && tempo_now < 200) {
			tempo_ary [ary_num] = tempo_now;
			ary_num++;
			if (ary_num >= tempo_ary.Length) {
				ary_num = 0;
			}
		}
		if (tempo_now < 250) {
			time_old = Time.time;
		}

		// calc average
		float tempo_total = 0;
		for (int i = 0; i < tempo_ary.Length; i++) {
			tempo_total += tempo_ary [i];
		}
		tempo_average = tempo_total / tempo_ary.Length;
		Debug.Log (tempo_average);
	}

	public float GetTempoAverage(){
		return tempo_average;
	}
}
