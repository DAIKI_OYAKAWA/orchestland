﻿using UnityEngine;
using System.Collections;

public class HandMotionCheck : MonoBehaviour {
	public HandController hand_controller;
	public TempoCheck tempoCheck;
	public float shakeInterval = 0.2f;
	public NatureSpawner spawner;

	GameObject[] hand_ary = new GameObject[2];
	Vector3[] old_position = new Vector3[2];
	Vector3 old_speed = new Vector3();
	float interval_count = 0f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 hand_speed = new Vector3();
		HandModel[] hands = hand_controller.GetAllPhysicsHands();
		foreach(HandModel hand in hands){
			bool find_hand = false;
			for (int i = 0; i < hand_ary.Length; i++) {
				if (hand.gameObject == hand_ary [i]) {
					find_hand = true;
					hand_speed += (hand.GetWristPosition() - old_position[i]);
					hand_ary [i] = hand.gameObject;
					old_position [i] = hand.GetWristPosition();
					break;
				}
			}
			if (!find_hand) {
				for (int i = 0; i < hand_ary.Length; i++) {
					if (hand_ary[i] == null) {
						hand_ary [i] = hand.gameObject;
						old_position [i] = hand.GetWristPosition();
						break;
					}
				}
			}
		}

		// shake check
		interval_count += Time.deltaTime;
		float move = (hand_speed - old_speed).magnitude;
		if (move > 0.2f && interval_count > shakeInterval) {
			interval_count = 0;
			Debug.Log ("shake:"+move);
			Shake ();
			tempoCheck.Beat ();
		}
		old_speed = hand_speed;
	}

	void OnDrawGizmos(){
		HandModel[] hands = hand_controller.GetAllPhysicsHands();
		foreach(HandModel hand in hands){
			Gizmos.color = Color.red;
			Gizmos.DrawWireSphere (hand.GetPalmPosition (), 1f);
			Gizmos.DrawRay (hand.GetPalmPosition(), old_speed*50);
		}
	}

	void Shake(){
		spawner.OnShake ();
	}
}
