﻿using UnityEngine;
using System.Collections;

public class NatureSpawner : MonoBehaviour {
	public Camera main_camera;
	public GameObject[] nature_prefab;
	public float distance = 15f;
	public Vector2 random_spawn = new Vector2(10,10);

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	public void OnShake(){
		int rand = Random.Range (0, nature_prefab.Length);
		GameObject obj = (GameObject)Instantiate (nature_prefab [rand]);
		Vector3 pos = main_camera.transform.position + main_camera.transform.forward * distance;
		pos.x += Random.Range (-random_spawn.x, random_spawn.x);
		pos.y = 0;
		pos.z += Random.Range (-random_spawn.y, random_spawn.y);
		obj.transform.position = pos;
		Vector3 angles = obj.transform.eulerAngles;
	}
}
