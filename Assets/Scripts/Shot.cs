﻿using UnityEngine;
using System.Collections;

public class Shot : MonoBehaviour {
	public HandController hand_controller;
	public GameObject bullet_prefab;

	int time = 0;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		time++;
		if (time > 60) {
			time = 0;
			HandModel[] hands = hand_controller.GetAllPhysicsHands();
			foreach(HandModel hand in hands){
				FingerModel[] fingers = hand.fingers;
				foreach(FingerModel finger in fingers){
					GameObject bullet = (GameObject)Instantiate(bullet_prefab);
					bullet.transform.position = finger.bones[3].position + finger.bones[3].forward  * 0.5f;
					bullet.GetComponent<Rigidbody>().velocity = finger.bones[3].forward * 5.0f;
				}
			}
		}
	}
}
