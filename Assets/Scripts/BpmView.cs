﻿using UnityEngine;
using System.Collections;

public class BpmView : MonoBehaviour {
	public TempoCheck tempoCheck;

	TextMesh text_mesh;
	// Use this for initialization
	void Start () {
		text_mesh = GetComponent<TextMesh> ();
	}
	
	// Update is called once per frame
	void Update () {
		text_mesh.text = Mathf.FloorToInt(tempoCheck.GetTempoAverage ()).ToString();
	}
}
